Websocket Event Loop
====================

[![pipeline status](https://gitlab.com/judahnator/websocket-event-loop/badges/master/pipeline.svg)](https://gitlab.com/judahnator/websocket-event-loop/commits/master)

This is a fairly straightforward library that extends my `judahnator/event-loop`
library, integrating with the `textalk/websocket` library.

The goal of this library was to create a simple event-loop that would
interact with websockets.

Usage
-----
The system might be simpler than you think, so before pulling hair out I
might suggest taking a quick look at the single test in this project.

Here is a quick example of how you can use it though.

```php
<?php

// pull in the composer autolaoder
require 'vendor/autoload.php';

use judahnator\WebsocketEventLoop\WebsocketEventLoop;

// Create a new instance of the class with your endpoint URL
$wssEventLoop = new WebsocketEventLoop('wss://echo.websocket.org');

// Optionally register any callbacks you want called before each event
$wssEventLoop->beforeEvent(function() {
    echo "An message has been received.".PHP_EOL;
});

// Register events that will accept the message received from the websocket server
$wssEventLoop->duringEvent(function($message) {
    echo "We have received {$message} from the websocket server.".PHP_EOL;
});

// You can conditionally halt the event loop by calling the halt method.
$wssEventLoop->duringEvent(function($message) use ($wssEventLoop) {
    if ($message === "something?") {
        $wssEventLoop->halt();
    }
});

// Optionally register events to be called after the message has been processed
$wssEventLoop->afterEvent(function() {
    echo "The message has been processed.".PHP_EOL;
});

// Add any periodic callbacks you want called.
// These are only ran if there are messages being processed.
$wssEventLoop->periodicCallback(function() {
    echo "This should be called once about every 10 seconds.";
}, 10);

// You can send messages back to the websocket server like this.
$wssEventLoop->sendMessage('ping');

// Call the run command to start processing messages
$wssEventLoop->run();

```

Extending
---------
This is just a thin wrapper for the `judahnator/event-loop` library,
so if you are interested in extending this library or the `WebsocketEventLoop`
class I recommend reading up on that library first.

Contributing
------------
I welcome contributions, just for the love of everything that is good
in the world write clean code.