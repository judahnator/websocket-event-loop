<?php

namespace judahnator\WebsocketEventLoop;


use judahnator\EventLoop\EventLoop;
use WebSocket\BadOpcodeException;
use WebSocket\Client;

class WebsocketEventLoop extends EventLoop
{

    private $client, $currentMessage;

    public function __construct(string $EndpointUrl, array $options = [])
    {
        $this->client = new Client($EndpointUrl, $options);
    }

    /**
     * Fetch the current message
     *
     * @return bool
     */
    public function getRunCondition(): bool
    {
        return ($this->currentMessage = $this->client->receive()) !== null;
    }

    /**
     * The result of this will be passed to the "during event" callbacks.
     *
     * @return mixed
     */
    public function getEventCallbackArgument()
    {
        return $this->currentMessage;
    }

    /**
     * Prepare the application.
     */
    public function loopSetup(): void
    {
        // No setup required
    }

    /**
     * Loop has finished, destruct the application
     */
    public function loopTeardown(): void
    {
        // No teardown required
    }

    /**
     * @param string $message
     * @throws BadOpcodeException
     */
    public function sendMessage(string $message) {
        $this->client->send($message);
    }

}