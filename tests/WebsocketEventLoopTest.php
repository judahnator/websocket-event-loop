<?php

namespace judahnator\WebsocketEventLoop\Tests;


use judahnator\WebsocketEventLoop\WebsocketEventLoop;
use PHPUnit\Framework\TestCase;

class WebsocketEventLoopTest extends TestCase
{

    public function testWssClass(): void
    {
        $wssLoop = new WebsocketEventLoop('wss://echo.websocket.org');

        $wssLoop->sendMessage('ping');

        $wssLoop->duringEvent(function($message) use ($wssLoop) {
            $this->assertEquals('ping', $message);
            $wssLoop->halt();
        });

        $wssLoop->run();
    }

}